# Library Manager API

this is a simple Library manager guided by [Postman document](https://documenter.getpostman.com/view/1056949/S1TbTFFc?version=latest#7e38c2a1-2c4a-436e-901a-73b26ca42a64)

**It has the following functinality**

 - user registration
 -  user login user 
 - borrow book 
 - user return book

**Tech used**
 [laravel](https://laravel.com/)
[jwt-auth libraray](https://jwt-auth.readthedocs.io/en/develop/)

### Get Started

 - `git clone https://github.com/fhulufhelo/Library-Manager-API.git`
 -  `cd Library-Manager-API`
 -  `composer install`
 -  `php artisan key:generate`
 - `mv .env.example .env` open `.env` and connect with you `database`
 - `php artisan migrate`

**Seeding data**

I already have dummy data to experince with [https://raw.githubusercontent.com/zygmuntz/goodbooks-10k/master/books.csv](https://raw.githubusercontent.com/zygmuntz/goodbooks-10k/master/books.csv)

Please run `php artisan db:seed` to populate `books` table with 100000 data
check this file for more `database/seeds/BookSeeder.php`

Now run `php artisan serve`  and start experience with app

![register](https://firebasestorage.googleapis.com/v0/b/project-one-d5e4b.appspot.com/o/Screenshot%20from%202020-04-20%2017-27-28.png?alt=media&token=2386817c-9614-43b7-b797-e730a212a6ad)


Now login `http://127.0.0.1:8000/api/auth/login`

![login](https://firebasestorage.googleapis.com/v0/b/project-one-d5e4b.appspot.com/o/login.png?alt=media&token=596f386d-8a40-4435-a4e2-08686194eb6e)

Now that you got your token please play around with the rest of the API 

[https://documenter.getpostman.com/view/1056949/S1TbTFFc?version=latest#740f772d-a3f5-41d7-b731-be686036591d](https://documenter.getpostman.com/view/1056949/S1TbTFFc?version=latest#740f772d-a3f5-41d7-b731-be686036591d)