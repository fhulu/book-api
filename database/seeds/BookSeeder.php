<?php

use Illuminate\Database\Seeder;

class BookSeeder extends Seeder
{
    const TITLE = 10;
    const ORIGINAL_TITLE = 9;
    const PUBLICATION_YEAR = 8;
    const ISBN = 4;
    const LANGUAGE_CODE = 11;
    const IMAGE = 21;
    const THUMBNAIL = 22;
    const AVERAGE_RATING = 16;
    const TOTAL_RATINGS = 17;

    const FILE_SRC = 'https://raw.githubusercontent.com/zygmuntz/goodbooks-10k/master/books.csv';
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new \App\Imports\FromCsv(self::FILE_SRC);

        //window OS, need txt ext
        // $data = new \App\Imports\ImportCsv(public_path('books.csv.txt'));

        foreach ($data->iteratorGenerator() as $key => $value) {

            if ($key === 0) {
                continue;
            }

            factory(App\Book::class)->create([
                'title' => $title = $value[self::TITLE],
                'original_title' =>  $value[self::ORIGINAL_TITLE],
                'publication_year' => $this->validateYear($value[self::PUBLICATION_YEAR]),
                'isbn' => intval($value[self::ISBN]),
                'language_code' => $value[self::LANGUAGE_CODE],
                'image' => $value[self::IMAGE],
                'thumbnail' => $value[self::THUMBNAIL],
                'avarage_rating' => intval($value[self::AVERAGE_RATING]),
                'total_ratings' => intval($value[self::TOTAL_RATINGS]),
            ]);
        }

    }

    private function validateYear($year)
    {
        $default = date("Y");

        if (checkdate(1, 1, $year = intval($year))) {
            return $year;
        }

        if ($year === 0) {
            return $default;
        }

        if ($year < 0 &&  (int) substr(abs($year), 0, 1) > 0) {
            $this->validateYear('1'.abs($year));
        }

        if (strlen($year) > 4) {
            $this->validateYear(substr(abs($year), 0, 4));
        }

      return $default;
    }
}
