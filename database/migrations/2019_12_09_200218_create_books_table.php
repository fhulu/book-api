<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',255);
            $table->string('original_title',255);
            $table->string('isbn',255);
            $table->smallInteger('publication_year');
            $table->string('language_code', 255);
            $table->text('image');
            $table->text('thumbnail');
            $table->integer('avarage_rating');
            $table->bigInteger('total_ratings');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
