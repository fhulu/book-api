<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $guarded = ['id'];

    public function users()
    {
        return $this->belongsToMany('App\User', 'user_books')
                ->withTimestamps()
                ->withPivot('due_at', 'return_at');
    }

    public function canCheckout(User $user)
    {
      return $this->users()->where('user_id', $user->id)->exists();
    }

}
