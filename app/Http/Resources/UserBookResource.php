<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Carbon;

class UserBookResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'book' => [
            "id" => $this->id,
            "title" => $this->title,
            "publication_year" => $this->publication_year,
            "isbn" => $this->isbn,
            "thumbnail" => $this->thumbnail,
            "average_rating" => $this->average_rating,
            "available" => true,
            "due_at" => Carbon::parse($this->pivot->due_at),
            "return_at" => (is_null($this->pivot->return_at))? null : Carbon::parse($this->pivot->return_at) ,
            ]
        ];
    }
    
}
