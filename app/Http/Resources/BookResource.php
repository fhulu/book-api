<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class BookResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "title" => $this->title,
            "original_title" => $this->original_title,
            "publication_year" => $this->publication_year,
            "isbn" => $this->isbn,
            "language_code" => $this->language_code,
            "image" => $this->image,
            "thumbnail" => $this->thumbnail,
            "average_rating" => $this->average_rating,
            "rating_count" => $this->total_ratings,
            "available" => true,
        ];
    }
}
