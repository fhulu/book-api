<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Carbon;

class UserBooksResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'book' => [
                "id" => $this->id,
                "title" => $this->title,
                "original_title" => $this->original_title,
                "publication_year" => $this->publication_year,
                "isbn" => $this->isbn,
                "language_code" => $this->language_code,
                "image" => $this->image,
                "thumbnail" => $this->thumbnail,
                "average_rating" => $this->average_rating,
                "total_ratings" => $this->total_ratings,
                "due_at" => $this->parseDateCheckout($this->users, 'due_at'),
                "return_at" => $this->parseDateCheckout($this->users, 'return_at') ,
            ]
        ];
    }

    private function parseDateCheckout($checkout, $date)
    {
        if (count($checkout) < 1){
            return null;
        }
        if (is_null($checkout[0]->pivot->$date)){
            return null;
        }
        return Carbon::parse($checkout[0]->pivot->$date);
    }
}
